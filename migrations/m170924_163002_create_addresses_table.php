<?php

use yii\db\Migration;

/**
 * Handles the creation of table `addresses`.
 */
class m170924_163002_create_addresses_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$this->createTable('addresses', [
			'id' => $this->primaryKey(),
			'userId'=>$this->integer(),
			'index' => $this->integer(5)->notNull(),
			'country' => $this->string(2)->notNull(),
			'city'=>$this->string()->notNull(),
			'street'=>$this->string()->notNull(),
			'houseNumber'=>$this->string()->notNull(),
			'apartmentNumber'=>$this->string(),
		]);
		$this->addForeignKey('userId','addresses','userId','users','id');
	}
	
	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable('addresses');
	}
}
