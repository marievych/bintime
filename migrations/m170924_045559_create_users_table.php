<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170924_045559_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
        	'id'=>$this->primaryKey(),
            'login'=>$this->string()->unique(),
	        'password'=>$this->string()->notNull(),
	        'firstName'=>$this->string()->notNull(),
	        'lastName'=>$this->string()->notNull(),
	        'gender'=>$this->smallInteger(1),
	        'createdAt'=>$this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
	        'email'=>$this->string()->unique()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
