<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 24.09.17
 * Time: 20:39
 */

namespace app\controllers;


use app\models\Address;
use app\models\User;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class AddressesController extends Controller
{
	public function actionEdit($id){
		$address=Address::findOne($id);
		if($address->load(Yii::$app->request->post()&&$address->validate())){
			$address->save();
			return $this->redirect(Url::toRoute(['user/details','id'=>$address->user->id]));
		}
		return $this->render('edit',['address'=>$address,'errors'=>$address->errors]);
	}
	
	public function actionCreate($userId){
		$address=new Address();
		$user=User::findOne($userId);
		if($address->load(Yii::$app->request->post())&&$address->validate()){
			$address->userId=$user->id;
			$address->save();
			return $this->redirect(Url::toRoute(['users/details','id'=>$userId]));
		}
		return $this->render('create', ['address' => $address,'user'=>$user]);
	}
	
	public function actionDelete($id){
		$address=Address::findOne($id);
		$user=$address->user;
		if(count($user->addresses)<2){
			Yii::$app->getSession()->setFlash('error',"User can't have less than 1 address.");
			return $this->redirect(Url::toRoute(['users/details','id'=>$user->id]));
		}
		if(Yii::$app->request->isPost){
			$address->delete();
			return $this->redirect(Url::toRoute(['users/details','id'=>$address->userId]));
		}
		return $this->render('delete');
	}
}