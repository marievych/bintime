<?php

namespace app\controllers;

use app\models\Address;
use app\models\User;
use app\models\UserSearch;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class UsersController extends Controller
{
	public function actionList()
	{
		$userSearchModel=new UserSearch();
		$filter=Yii::$app->request->get();
		$provider=$userSearchModel->search($filter);
		return $this->render('list', ['provider' => $provider,'filterModel'=>$userSearchModel]);
	}
	
	public function actionEdit($id)
	{
		$user = User::findOne($id);
		$user->setScenario('update');
		if ($user->load(Yii::$app->request->post()) && $user->validate()) {
			$user->save(false);
			return $this->redirect(Url::toRoute(['users/details', 'id' => $user->id]));
		}
		//print_r($user->errors);
		return $this->render('edit', ['user' => $user]);
	}
	
	public function actionDetails($id)
	{
		$user = User::findOne($id);
		$addressesProvider = new ActiveDataProvider([
			'query' => $user->getAddresses(),
			'pagination' => [
				'pageSize' => 5
			]
		]);
		return $this->render('details', ['user' => $user, 'addressesProvider' => $addressesProvider]);
	}
	
	public function actionCreate()
	{
		$user = new User();
		$user->setScenario('create');
		$address = new Address();
		$address->setScenario('create');
		if ($user->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $user->validate() && $address->validate()) {
			$user->password=Yii::$app->security->generatePasswordHash(Yii::$app->request->post('password'));
			$user->save(false);
			$address->userId = $user->id;
			$address->save(false);
			return $this->redirect(Url::toRoute(['users/details', 'id' => $user->id]));
		}
		return $this->render('create', ['user' => $user, 'address' => $address]);
	}
	
	public function actionDelete($id){
		$user=User::findOne($id);
		if(Yii::$app->request->isPost){
			foreach ($user->addresses as $address){
				$address->delete();
			}
			$user->delete();
			return $this->redirect(Url::toRoute(['users/list']));
		}
		return $this->render('delete', ['user' => $user]);
	}
}
