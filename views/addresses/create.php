<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 25.09.17
 * Time: 7:19
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'New address';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-6 col-md-8 col-lg-offset-3 col-md-offset-2">
		<h2>New Address for <?= $user->fullName ?></h2>
		<? $form = ActiveForm::begin() ?>
		<?= $form->field($address, 'index')->textInput() ?>
		<?= $form->field($address, 'country')->textInput() ?>
		<?= $form->field($address, 'city')->textInput() ?>
		<?= $form->field($address, 'street')->textInput() ?>
		<?= $form->field($address, 'houseNumber')->textInput() ?>
		<?= $form->field($address, 'apartmentNumber')->textInput() ?>
		<?= Html::submitButton('Create', ['class' => 'btn btn-primary pull-right']) ?>
		<? ActiveForm::end() ?>
	</div>
</div>