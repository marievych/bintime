<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 24.09.17
 * Time: 20:38
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Edit address';
$this->params['breadcrumbs'][] = $this->title;
?>
<? if ($errors) {
	print_r($errors);
} ?>
<div class="row">
	<div class="col-lg-6 col-md-8 col-lg-offset-3 col-md-offset-2">
		<h2>Edit address</h2>
		<? $form = ActiveForm::begin() ?>
		<?= $form->field($address, 'index')->textInput(['value' => $address->indexString]) ?>
		<?= $form->field($address, 'country')->textInput() ?>
		<?= $form->field($address, 'city')->textInput() ?>
		<?= $form->field($address, 'street')->textInput() ?>
		<?= $form->field($address, 'houseNumber')->textInput() ?>
		<?= $form->field($address, 'apartmentNumber')->textInput() ?>
		<?= Html::submitButton('Save', ['class' => 'btn btn-success pull-right']) ?>
		<? ActiveForm::end() ?>
	</div>
</div>