<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 24.09.17
 * Time: 19:59
 */

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = $user->fullName;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
	<h3><?= $user->fullName ?> detail info.</h3>
	<?= DetailView::widget([
		'model' => $user,
		'attributes' => [
			'id',
			'login',
			'firstName',
			'lastName',
			'genderString',
			'createdAt:datetime',
			'email'
		]
	]) ?>
	<?= Html::a('Edit user', Url::toRoute(['users/edit', 'id' => $user->id]), ['class' => 'btn btn-primary pull-right']) ?>
</div>
<div class="container">
	<h4>Addresses</h4>
	<?= GridView::widget([
		'dataProvider' => $addressesProvider,
		'columns' => [
			[
				'attribute' => 'index',
				'value'=>'indexString'
			],
			'country',
			'city',
			'street',
			'houseNumber',
			'apartmentNumber',
			[
				'label' => 'Actions',
				'content' => function ($address) {
					return '<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li>' . Html::a("Edit", Url::toRoute(["addresses/edit", "id" => $address->id])) . '</li>
    <li>' . Html::a("Delete", Url::toRoute(["addresses/delete", "id" => $address->id])) . '</li>
  </ul>
</div>';
				}
			]
		]
	]) ?>
	<?= Html::a('New address', Url::toRoute(['addresses/create', 'userId' => $user->id]), ['class' => 'btn btn-primary pull-right']) ?>
</div>