<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 24.09.17
 * Time: 9:11
 */

use app\models\User;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
	<?= GridView::widget([
		'filterModel' => $filterModel,
		'dataProvider' => $provider,
		'columns' => [
			[
				'attribute' => 'login',
				'content' => function ($user) {
					return Html::a($user->login, Url::toRoute(['users/details', 'id' => $user->id]));
				}
			],
			'firstName',
			'lastName',
			'email',
			[
				'attribute' => 'gender',
				'class' => DataColumn::className(),
				'value' => function ($user) {
					return $user->genderString;
				},
				'filter' => [User::MALE => 'male', User::FEMALE => 'female', User::OTHER => 'other']
			],
			'createdAt:datetime',
			[
				'label' => 'Actions',
				'format' => 'html',
				'content' => function ($user) {
					return '<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li>' . Html::a("Edit", Url::toRoute(["users/edit", "id" => $user->id])) . '</li>
    <li>' . Html::a("Delete", Url::toRoute(["users/delete", "id" => $user->id])) . '</li>
  </ul>
</div>';
				}
			]
		]
	
	]) ?>
	<?= Html::a('New user', Url::toRoute(['users/create']), ['class' => 'btn btn-primary pull-right']) ?>
