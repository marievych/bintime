<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 25.09.17
 * Time: 13:08
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\bootstrap\Html;

$this->title = "Delete $user->fullName";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title">Delete <?= $user->fullName?></div>
	</div>
	<div class="panel-body">
		<?$form=ActiveForm::begin()?>
		<div class="form-group">
			<h4>Are you sure you want to delete <?= $user->fullName ?>?</h4>
		</div>
		<div class="form-group">
			<?=Html::a('Cancel',Yii::$app->request->referrer,['class'=>'btn btn-primary'])?>
		<?=Html::submitButton('Delete',['class'=>'btn btn-danger pull-right'])?>
		</div>
		<?ActiveForm::end()?>
	</div>
</div>