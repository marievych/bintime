<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 24.09.17
 * Time: 11:39
 */

use app\models\User;
use yii\bootstrap\ActiveForm;
$this->title = "Edit $user->fullName";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-6 col-md-8 col-lg-offset-3 col-md-offset-2">
<h2>Edit <?=$user->login?></h2>
<? $form=ActiveForm::begin()?>
<?=$form->field($user,'login')->textInput()?>
<?=$form->field($user,'password')->passwordInput(['value'=>''])?>
<?=$form->field($user,'repeatPassword')->passwordInput()?>
<?=$form->field($user,'firstName')->textInput()?>
<?=$form->field($user,'lastName')->textInput()?>
<?=$form->field($user,'gender')->dropDownList([User::MALE=>'male',User::FEMALE=>'female',User::OTHER=>'other'])?>
<?=$form->field($user,'email')->textInput()?>
<?= \yii\bootstrap\Html::submitButton('Save',['class'=>'btn btn-success pull-right'])?>
<? ActiveForm::end() ?>
	</div>
</div>