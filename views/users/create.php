<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 25.09.17
 * Time: 7:48
 */

use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'New user';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-6 col-md-8 col-lg-offset-3 col-md-offset-2">
		<? $form = ActiveForm::begin() ?>
		<div>
			<h2>New user</h2>
			<?= $form->field($user, 'login')->textInput() ?>
			<?= $form->field($user, 'password')->passwordInput() ?>
			<?= $form->field($user, 'repeatPassword')->passwordInput() ?>
			<?= $form->field($user, 'firstName')->textInput() ?>
			<?= $form->field($user, 'lastName')->textInput() ?>
			<?= $form->field($user, 'gender')->dropDownList([User::MALE => 'male', User::FEMALE => 'female', User::OTHER => 'other']) ?>
			<?= $form->field($user, 'email')->input('email') ?>
		
		</div>
		
		<div>
			<h3>User address</h3>
			<?= $form->field($address, 'index')->textInput() ?>
			<?= $form->field($address, 'country')->textInput() ?>
			<?= $form->field($address, 'city')->textInput() ?>
			<?= $form->field($address, 'street')->textInput() ?>
			<?= $form->field($address, 'houseNumber')->textInput() ?>
			<?= $form->field($address, 'apartmentNumber')->textInput() ?>
			<?= Html::submitButton('Create', ['class' => 'btn btn-success pull-right']) ?>
		</div>
		<? ActiveForm::end() ?>
	</div>
</div>