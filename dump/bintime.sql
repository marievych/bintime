-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: yii
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `index` int(5) NOT NULL,
  `country` varchar(2) NOT NULL,
  `city` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `houseNumber` varchar(255) NOT NULL,
  `apartmentNumber` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `userId` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (8,6,2137,'UA','Kyiv','Very','7','22'),(9,7,3123,'UA','Kyiv','Lvivska','23','1'),(10,8,3125,'UA','Kyiv','Serbska','12','34'),(11,9,1026,'UA','Kyiv','Libidska','28','1'),(12,10,4205,'UA','Kyiv','Kopernyka','29','53'),(13,11,2145,'UA','Kyiv','Sonyachna','21','3'),(14,12,4205,'UA','Kyiv','Obolonskyi avenue','35',''),(15,13,1187,'UA','Kyiv','Mlechna','12','56'),(16,16,1176,'UA','Kyiv','Raskova','123','43'),(17,17,4123,'UA','Lviv','Serbska','12','4'),(18,18,4125,'UA','Lviv','Mala','23',''),(20,6,2138,'UA','Lviv','Zakrevska','89','1'),(21,6,4137,'UA','Odesa','Karabina','34','2a'),(22,6,5138,'UA','Kyiv','Marka','56','7b'),(26,24,22222,'US','Washington','George Washington','1',''),(27,6,2002,'UA','Horishni Plavni','Nezalezhnosti','23','1'),(28,6,36542,'UA','Vinnytsia','Keletska','4',''),(29,25,22222,'US','Disney Land','Mickey Mouse','1',''),(30,26,66666,'RU','Rostov','Lenina','13',''),(31,27,1111,'UA','South Park','Central','39','');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1506314335),('m170924_045559_create_users_table',1506314337),('m170924_163002_create_addresses_table',1506314337);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `gender` smallint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,'Tolik7','','Anatoliy','Puk',0,'2017-09-25 19:48:09','tolik@ukr.net'),(7,'qawa1','$2y$13$IEpdfU1x5hlsqDv5HObqH.kO306zYRYetyJoFYsWXuYJ2d2OawmhS','Gigi','May',1,'2017-09-25 19:50:31','gigi@ukr.net'),(8,'pink24','$2y$13$lJj4Iik1iKF7eN0KjI.fguOM7nFsA/PXZDUmW.UVhye26SHOY0o72','Pigi','Wern',0,'2017-09-25 19:52:44','pigi@ukr.net'),(9,'tililyp','$2y$13$8GkXvIJFERut88coi/CiNOlF1KwTF0BkVzKAL8vhQa1DAImJVk4fe','Tilin','Morb',0,'2017-09-25 19:56:06','tilili@ukr.net'),(10,'marievych','$2y$13$gwKfgnXXBltZkr76mhZX6.94NT9cSZaj0eHgSpIYBPxzNKa45FUK6','Yevhen','Marievych',0,'2017-09-25 19:56:43','yevhen.marievych@gmail.com'),(11,'zibi567','$2y$13$EKD6vfug8x3.yCvxqT4AROiJDSRw7E7FjFhs9b8M95Rm88yILWE1e','Zenda','Pik',1,'2017-09-25 19:58:08','zibi@ukr.net'),(12,'vasia','345435643','Vasiliy','Pupkin',0,'2017-09-25 20:00:03','vasia2@gmail.com'),(13,'garik21','$2y$13$BSX/Pbw834p.VbIq4RlVoOit.ojzk4EEXOaoSyziwieaLH6c2dxTO','Garri','Lemon',0,'2017-09-25 20:00:41','lemon@ukr.net'),(16,'xena234','$2y$13$56pPTs4r6TZbqOH/9rl5J.7itp1BgsRhBbnFMbqDkqBKmWR0f6xxK','Xena','Morgan',1,'2017-09-25 20:04:46','xena@ukr.net'),(17,'Jenkins','$2y$13$MBkbP3jv2LJ3h8iUDQyJAeuwAE5ozGKCWAKszLs5fCzm80Z5PA4Hu','Jeki','Fish',0,'2017-09-25 20:07:22','jeqwe@ukr.net'),(18,'rembo','$2y$13$Kl2TvrDoVaWMb5p9Lc46yuRxLEddxmOdYBWBRA968GtvlZ5Lhagq.','Remi','Duck',0,'2017-09-25 20:09:47','rembo@ukr.net'),(24,'gates','$2y$13$q7VaQU9BFZiwAMAqaKMcxOqdn9LjiqeEsEgU/A1A3jfTGCIMK1uXq','Bill','Gates',0,'2017-09-26 07:27:20','gates@gmail.com'),(25,'mickey','','Mickey','Mouse',2,'2017-09-26 09:22:32','mickey@gmail.com'),(26,'yanukovych','$2y$13$n/bhp2pw9t9LoIqP9n5MU.q5SwHmteaFvbtNGIagziBlOifLElJ3S','Viktor','Yanukovych',2,'2017-09-26 09:38:52','yanukovych@rostov.ru'),(27,'cartman','$2y$13$9DXnd3Kf6F6kPUvWD9hYVO5NV3D9twaM7hQV16tXaCCMxh02juy/C','Eric','Cartman',0,'2017-09-26 09:48:02','douchebag@yahoo.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-26 13:18:36
