<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 25.09.17
 * Time: 15:45
 */

namespace app\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class UserSearch extends Model
{
	public $login;
	public $firstName;
	public $lastName;
	public $email;
	public $gender;
	
	public function rules()
	{
		return [
			[['login','firstName','lastName','email'],'string'],
			[['gender'],'integer']
		];
	}
	
	
	public function search($filter)
	{
		$query=User::find();
		$dataProvider=new ActiveDataProvider([
			'query'=>$query,
			'pagination'=>new Pagination(['pageSize'=>10])
		]);
		if($this->load($filter)) {
			
			if ($this->login) {
				$query->andFilterWhere(['like', '{{users}}.login', $this->login]);
			}
			if($this->firstName){
				$query->andFilterWhere(['like','{{users}}.firstName',$this->firstName]);
			}
			if($this->lastName){
				$query->andFilterWhere(['like','{{users}}.lastName',$this->lastName]);
			}
			if($this->email){
				$query->andFilterWhere(['like','{{users}}.email',$this->email]);
			}
			if(isset($this->gender)){
				$query->andFilterWhere(['{{users}}.gender'=>$this->gender]);
			}
		}
		return $dataProvider;
	}
}