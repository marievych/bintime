<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $login
 * @property string $firstName
 * @property string $lastName
 * @property int $gender
 * @property \DateTime $createdAt
 * @property string $email
 * @property string $authKey
 * @property string $password
 */
class User extends ActiveRecord
{
	const MALE = 0;
	const FEMALE = 1;
	const OTHER = 2;
	
	public $repeatPassword;
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'users';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['gender'], 'integer'],
			[['createdAt'], 'safe'],
			[['password','repeatPassword'],'required'],
			[['password'],'string','min'=>6,'max' => 255],
			[['login', 'firstName', 'lastName', 'email'], 'string', 'max' => 255],
			[['login'], 'unique','when' => function($model,$attribute){
			return $model->{$attribute}!==$model->getOldAttribute($attribute);
			}],
			[['email'], 'unique','when' => function($model,$attribute){
				return $model->{$attribute}!==$model->getOldAttribute($attribute);
			}],
			[['repeatPassword'],'compare','compareAttribute' => 'password','message' => 'Passwords are not equal.']
		];
	}
	
	public function scenarios()
	{
		return [
			'create'=>[
				'login',
				'password',
				'repeatPassword',
				'firstName',
				'lastName',
				'gender',
				'email'
			],
			'update'=>[
				'login',
				'password',
				'repeatPassword',
				'firstName',
				'lastName',
				'gender',
				'email'
			]
		];
	}
	
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'login' => 'Login',
			'firstName' => 'First Name',
			'lastName' => 'Last Name',
			'gender' => 'Gender',
			'genderString' => 'Gender',
			'createdAt' => 'Created At',
			'email' => 'Email',
			'password' => 'Password',
		];
	}
	
	public function setGender($value)
	{
		$this->attributes['gender'] = $value;
	}
	
	
	public function getGenderString()
	{
		switch ($this->gender) {
			case self::MALE: {
				return 'male';
			}
			case self::FEMALE: {
				return 'female';
			}
			case self::OTHER: {
				return 'other';
			}
		}
	}
	
	public function getFullName()
	{
		return "$this->firstName $this->lastName";
	}
	
	public function getAddresses()
	{
		return $this->hasMany(Address::className(), ['userId' => 'id']);
	}
	
	public function beforeSave($insert)
	{
		$this->firstName=ucfirst($this->firstName);
		$this->lastName=ucfirst($this->lastName);
		return parent::beforeSave($insert);
	}
}
