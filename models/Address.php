<?php
/**
 * Created by PhpStorm.
 * User: marievych
 * Date: 24.09.17
 * Time: 20:06
 */

namespace app\models;


use yii\db\ActiveRecord;

class Address extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'addresses';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['index'], 'match','pattern'=>'/\d{5}/'],
			[['country'], 'match','pattern' => '/[A-Z]{2}/','message' => 'Country should contain 2 capitalized letters.'],
			[['city'],'string'],
			[['street'], 'string'],
			[['houseNumber'], 'string'],
			[['apartmentNumber'],'string']
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'index'=>'Index',
			'indexString'=>'Index',
			'country'=>'Country',
			'city'=>'City',
			'street'=>'Street',
			'houseNumber'=>'House number',
			'apartmentNumber'=>'Apartment (office) number'
		];
	}
	
	public function getUser(){
		return $this->hasOne(User::className(),['id'=>'userId']);
	}
public function getIndexString(){
	return sprintf("%05d", $this->index);
}
}